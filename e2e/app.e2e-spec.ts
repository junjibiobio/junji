import { FrontEndJunjiPage } from './app.po';

describe('front-end-junji App', function() {
  let page: FrontEndJunjiPage;

  beforeEach(() => {
    page = new FrontEndJunjiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
