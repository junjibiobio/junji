import { Component, OnInit } from '@angular/core';

import { Funcionario } from './funcionario';
import { FuncionarioService} from './funcionario.service';

@Component({
  moduleId: module.id,		
  selector: 'app-funcionario',
  templateUrl: './funcionario.component.html',
  providers: [ FuncionarioService ],
  styleUrls: ['./funcionario.component.css']
})
export class FuncionarioComponent implements OnInit {
	errorMessage: string;
	funcionarios: Funcionario[];
	mode= 'Promise';

  constructor(private funcionarioService: FuncionarioService) { }

  ngOnInit() {
  	this.getFuncionarios();
  }

  getFuncionarios(){
  	this.funcionarioService.getFuncionarios().then(funcionarios => this.funcionarios = funcionarios , error => this.errorMessage = <any>error);
  }

  



  

}
