import { Injectable } from '@angular/core';
import { Headers,Http } from '@angular/http';


import 'rxjs/add/operator/toPromise';

import { Funcionario } from './funcionario';

@Injectable()
export class FuncionarioService{
	private headers = new Headers({'Content-Type': 'application/json'});
	private funcionarioUrl = 'http://localhost:8080/ServicioWeb/funcionario';

	constructor(private http: Http){}

	getFuncionarios(): Promise<Funcionario[]>{
		return this.http.get(this.funcionarioUrl)
		.toPromise().then(response => response.json()).catch(this.handleError);
	}

  getFuncionario(id: number): Promise<Funcionario[]>{
    const url = `${this.funcionarioUrl}/${id}`;
    return this.http.get(url)
    .toPromise()
    .then(response => response.json()).catch(this.handleError);
  }

  

	private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
  
}