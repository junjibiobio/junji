import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';

import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Funcionario }           from './funcionario';

@Injectable()
export class FuncionarioSearchService{
	private funcionarioUrl = 'http://localhost:8080/ServicioWeb/funcionario';

	constructor(private http: Http) {}

	search(term: string): Observable<Funcionario[]>{
		return this.http
			.get(`http://localhost:8080/ServicioWeb/funcionario/buscar/${term}`)
			.map(response => response.json());
	}

}