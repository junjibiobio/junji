export class Funcionario{
	id: number;
	rutFuncionario: string;
	dvRutFuncionario: number;
	nombresFuncionario: string;
	apellidopFuncionario: string;
	apellidomFuncionario: string;
	correoFuncionario: string;	
}