import 'rxjs/add/operator/switchMap';
import { Component, OnInit }      from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location }               from '@angular/common';

import { Funcionario } from './funcionario';
import { FuncionarioService} from './funcionario.service';


@Component({
  moduleId: module.id,
  selector: 'funcionario-detail',
  templateUrl : './funcionario.detail.component.html',
  providers: [ FuncionarioService ],
  styleUrls : [ './funcionario.component.css' ]
})

export class FuncionarioDetailComponent implements OnInit{
	funcionario: Funcionario[];

	constructor(private funcionarioService: FuncionarioService,
		private route : ActivatedRoute,
		private location: Location){}

	ngOnInit(){
		this.route.params.switchMap((params: Params) => this.funcionarioService.getFuncionario(+params['id']))
		.subscribe(funcionario => this.funcionario = funcionario);
	}


}
