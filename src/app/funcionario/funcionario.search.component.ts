import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

import { Observable }        from 'rxjs/Observable';
import { Subject }           from 'rxjs/Subject';

// Observable class extensions
import 'rxjs/add/observable/of';

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { FuncionarioSearchService } from './funcionario.search.service';
import { Funcionario } from './funcionario';

@Component({
  moduleId: module.id,
  selector: 'funcionario-search',
  templateUrl: './funcionario.search.component.html',
  styleUrls: [ './funcionario.component.css' ],
  providers: [FuncionarioSearchService]
})

export class FuncionarioSearchComponent implements OnInit{
	funcionarios: Observable<Funcionario[]>;
	private searchTerms = new Subject<string>();

	constructor(
				private funcionarioSearchService: FuncionarioSearchService,
				private router: Router
				){		
	}


	search(term: string): void{
		this.searchTerms.next(term);
	}

	ngOnInit(): void {
		this.funcionarios = this.searchTerms
		.debounceTime(300)
		.distinctUntilChanged()
		.switchMap( term => term
			? this.funcionarioSearchService.search(term)
			: Observable.of<Funcionario[]>([]))
		.catch(error => {
			console.log(error);
			return Observable.of<Funcionario[]>([]);

		});

	}

	gotoDetail(funcionario: Funcionario): void{
		let link = ['/funcionario/', funcionario.id];
		this.router.navigate(link);
	}
}