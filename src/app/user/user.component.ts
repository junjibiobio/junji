import { Component, OnInit } from '@angular/core';

import { User } from './user';
import { UserService } from './user.service';


@Component({
	moduleId: module.id,
  selector: 'app-user',
  templateUrl: './user.component.html',
  providers: [ UserService ],
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  errorMessage: string;
	users: User[];
  mode = 'Promise';
  constructor(private userService: UserService,
  	) { }

  ngOnInit() { 
    this.getUsers();  
    } 

  getUsers(){
  	this.userService.getUsers().then(users => this.users = users , error => this.errorMessage = <any>error);
  }

  
 

}
