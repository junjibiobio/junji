import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { EstablecimientoComponent} from './establecimiento/establecimiento.component';
import { UserComponent } from './user/user.component';
import { FuncionarioComponent } from './funcionario/funcionario.component';
import { FuncionarioDetailComponent} from './funcionario/funcionario.detail.component';



const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  {path: 'dashboard',  component: DashboardComponent },
  {path: 'establecimiento',  component: EstablecimientoComponent },
  {path: 'user', component: UserComponent},
  {path: 'funcionario', component: FuncionarioComponent},
  {path: 'funcionario/:id', component: FuncionarioDetailComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}