import { Injectable } from '@angular/core';
import { Headers, Http} from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Establecimiento } from './establecimiento';

@Injectable()
export class EstablecimientoService{
	private headers = new Headers({'Content-Type': 'application/json'});
  	private establecimientosUrl = 'http://localhost:8080/ServicioWeb/establecimientos/';  // URL to web api

  	constructor(private http: Http){}

  	getEstablecimientos(): Promise<Establecimiento[]>{
  		return this.http.get(this.establecimientosUrl)
  				.toPromise()
  				.then(response => response.json().data as Establecimiento[])
  				.catch(this.handleError);
  	}

  	getEstablecimiento(id: number): Promise<Establecimiento>{
  		const url = '${this.establecimientoUrl}/${id}';
  		return this.http.get(url)
  			.toPromise()
      		.then(response => response.json().data as Establecimiento)
      		.catch(this.handleError);
  	}

  	delete(id: number): Promise<void> {
    const url = '${this.establecimientosUrl}/${id}';
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(name: string): Promise<Establecimiento> {
    return this.http
      .post(this.establecimientosUrl, JSON.stringify({name: name}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  update(establecimiento: Establecimiento): Promise<Establecimiento> {
    const url = `${this.establecimientosUrl}/${establecimiento.id}`;
    return this.http
      .put(url, JSON.stringify(establecimiento), {headers: this.headers})
      .toPromise()
      .then(() => establecimiento)
      .catch(this.handleError);
  }



  	private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}