import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

import { Establecimiento } from './establecimiento';
import { EstablecimientoService } from './establecimiento.service';

@Component({
  moduleId: module.id,	
  selector: 'app-establecimiento',
  templateUrl: './establecimiento.component.html',
  styleUrls: ['./establecimiento.component.css']
})
export class EstablecimientoComponent implements OnInit {
	establecimientos: Establecimiento[];
	selectedEstablecimiento: Establecimiento;

  constructor(
  	private establecimientoService: EstablecimientoService,
  	private router: Router) { }

  getEstablecimientos(): void{
  	this.establecimientoService
  		.getEstablecimientos()
  		.then(establecimientos => this.establecimientos = establecimientos);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.establecimientoService.create(name)
      .then(establecimiento => {
        this.establecimientos.push(establecimiento);
        this.selectedEstablecimiento = null;
      });
  }

  delete(establecimiento: Establecimiento): void {
    this.establecimientoService
        .delete(establecimiento.id)
        .then(() => {
          this.establecimientos = this.establecimientos.filter(h => h !== establecimiento);
          if (this.selectedEstablecimiento === establecimiento) { this.selectedEstablecimiento = null; }
        });
  }

  ngOnInit(): void {
  	this.getEstablecimientos();
  }

  onSelect(establecimiento: Establecimiento): void {
    this.selectedEstablecimiento = establecimiento;
  }

  gotoDetail(): void{
  	this.router.navigate(['/detail', this.selectedEstablecimiento.id]);
  }

}
