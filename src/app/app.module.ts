import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule} from '@angular/http';



import { AppRoutingModule } from './app-routing.module';




import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EstablecimientoComponent } from './establecimiento/establecimiento.component';
import { EstablecimientoService } from './establecimiento/establecimiento.service';
import { UserComponent } from './user/user.component';
import { FuncionarioComponent } from './funcionario/funcionario.component';
import { FuncionarioDetailComponent } from './funcionario/funcionario.detail.component';
import { FuncionarioSearchComponent } from './funcionario/funcionario.search.component';



@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    EstablecimientoComponent,
    UserComponent,
    FuncionarioComponent,
    FuncionarioDetailComponent,
    FuncionarioSearchComponent    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
